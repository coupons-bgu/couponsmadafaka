﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;

namespace BL
{
    public class Functions
    {
        static MyDataBaseDataContext db = new MyDataBaseDataContext();

        public static Boolean checkCouponSearchTerms(String CatID, String Serial, String Rating, String minPrice, String maxPrice, String Category)
        {
            Boolean correct = true;
            if ((!IsDigitsOnly(CatID)) || (!IsDigitsOnly(Serial)) ||
                (!IsDigitsOnly(minPrice)) || (!IsDigitsOnly(maxPrice)) || (!IsDigitsOnly(Category)))
                correct = false;

            return correct;
        }

        public static Boolean OrderCoupon(Coupon c)
        {
            Boolean toRet = true;
            Coupons_of_Client coc = new Coupons_of_Client();
            coc.Client = LoggedInUser.getUserName();
            coc.Coupon_Serial_Number = c.Serial_Number;
            try
            {
                db.Coupons_of_Clients.InsertOnSubmit(coc);
                db.SubmitChanges();
            }
            catch
            {
                toRet = false;
            }
            return toRet;
        }

        public static List<Coupon> SearchCoupons(String CatID, String Serial, String Rating, String minPrice, String maxPrice, String Category)
        {
            List<Coupon> toReturn = new List<Coupon>();

            foreach (Coupon c in db.Coupons)
                if (((String.Compare(CatID, "") == 0) || (c.categoryID == int.Parse(CatID))) &&
                       ((String.Compare(Serial, "") == 0) || (c.Serial_Number == int.Parse(Serial))) &&
                       ((String.Compare(Rating, "No Rating") == 0) || (c.rating == int.Parse(Rating))) &&
                       ((String.Compare(minPrice, "") == 0) || (c.discountedPrice >= int.Parse(minPrice))) &&
                       ((String.Compare(maxPrice, "") == 0) || (c.discountedPrice <= int.Parse(maxPrice))) &&
                       ((String.Compare(Category, "") == 0) || (c.couponCategory == int.Parse(Category))) &&
                        c.isApproved == true)
                    toReturn.Add(c);
            return toReturn;
        }

        public static List<Coupon> generateCoupons()
        {
            var temp = (from coc in db.Coupons_of_Clients
                        where (String.Compare(coc.Client, LoggedInUser.getUserName()) == 0)
                        select coc).ToList();

            var q = (from ser in temp
                     join c in db.Coupons on ser.Coupon_Serial_Number equals c.Serial_Number
                     select c).ToList();

            return q;
        }

        public static List<Coupon> generateUnApprovedCoupons()
        {

            var q = (from c in db.Coupons
                     where c.isApproved==false
                     select c).ToList();

            return q;
        }

        public static List<Business> generateBusinessesByOwner()
        {
            var temp = (from bobo in db.Businesses_of_Business_Owners
                        where (String.Compare(bobo.Business_Owner, LoggedInUser.getUserName()) == 0)
                        select bobo).ToList();

            var q = (from ser in temp
                     join c in db.Businesses on ser.Business equals c.business_name
                     select c).ToList();

            return q;
        }

        public static List<Coupon> generateCouponsForBusinesses(Business business)
        {
            var temp = (from cfb in db.Coupons_for_Businesses
                        where (String.Compare(cfb.Business, business.business_name) == 0)
                        select cfb).ToList();

            var q = (from ser in temp
                     join c in db.Coupons on ser.Coupon equals c.Serial_Number
                     select c).ToList();

            return q;
        }

        public static List<Business> generateBusinesses()
        {
           
            var q = (from business in db.Businesses
                     select business).ToList();

            return q;
        }

        public static void addClient(String username, String FName, String LName, String Password, String Email, String PhoneNumber, DateTime DoB, String Gender)
        {
            Client toAdd = new Client();
            toAdd.username = username;
            toAdd.first_name = FName;
            toAdd.last_name = LName;
            toAdd.password = Password;
            toAdd.email = Email;
            toAdd.phone = PhoneNumber;
            toAdd.birth_day = DoB;
            if (String.Compare(Gender, "Male") == 0)
                toAdd.gender = 1;
            else
                toAdd.gender = 0;
            db.Clients.InsertOnSubmit(toAdd);
            db.SubmitChanges();
        }

        public static void addBusinessOwner(String username, String FName, String LName, String Password, String Email, String PhoneNumber, DateTime DoB, String Gender)
        {
            Business_Owner toAdd = new Business_Owner();
            toAdd.username = username;
            toAdd.first_name = FName;
            toAdd.last_name = LName;
            toAdd.password = Password;
            toAdd.email = Email;
            toAdd.phone = PhoneNumber;
            toAdd.birth_date = DoB;
            if (String.Compare(Gender, "Male") == 0)
                toAdd.gender = 1;
            else
                toAdd.gender = 0;
            db.Business_Owners.InsertOnSubmit(toAdd);
            db.SubmitChanges();
        }

        public static void addCoupon(String cat, String CatID, String CName, String Desc, String OPrice, String DPrice, String SNumber, String Rating, DateTime LD, Business business)
        {
            Coupon toAdd = new Coupon();
            toAdd.couponCategory = int.Parse(cat);
            toAdd.categoryID = int.Parse(CatID);
            toAdd.name = CName;
            toAdd.description = Desc;
            toAdd.orginialPrice = int.Parse(OPrice);
            toAdd.discountedPrice = int.Parse(DPrice);
            toAdd.lastDate = LD;
            toAdd.Serial_Number = int.Parse(SNumber);
            toAdd.rating = int.Parse(Rating);
            toAdd.isApproved = false;
            db.Coupons.InsertOnSubmit(toAdd);
            db.SubmitChanges();
            Coupons_for_Business cfb = new Coupons_for_Business();
            cfb.Business = business.business_name;
            cfb.Coupon = toAdd.Serial_Number;
            db.Coupons_for_Businesses.InsertOnSubmit(cfb);
            db.SubmitChanges();
        }

        public static Boolean TryAddNewCoupon(String SNumber)
        {
            Boolean exist = false;
            foreach (Coupon c in db.Coupons)
            {
                if((!exist) && (c.Serial_Number == int.Parse(SNumber)))
                    exist = true;
            }
            return exist;
        }

        public static Boolean TryAddNewUser(String username, String email)
        {
            Boolean exist = false;
            foreach (Client c in db.Clients)
            {
                    if ((!exist) && ((String.Compare((c.username).Trim(), username) == 0) || (String.Compare((c.email).Trim(), email) == 0)))
                    {
                        exist = true;
                    }
            }
            if (exist == false)
            {
                foreach (Business_Owner BO in db.Business_Owners)
                {
                        if ((!exist) && ((String.Compare((BO.username).Trim(), username) == 0) || (String.Compare((BO.email).Trim(), email) == 0)))
                        {
                            exist = true;
                        }
                }
            }

            if (exist == false)
            {
                foreach (System_Manager SM in db.System_Managers)
                {
                        if ((!exist) && ((String.Compare((SM.username).Trim(), username) == 0) || (String.Compare((SM.email).Trim(), email) == 0)))
                        {
                            exist = true;
                        }
                }
            }

            return exist;
        }

        public static Boolean TryForgotPassword(String email)
        {
            Boolean ret = false;
            foreach (Client c in db.Clients)
            {
                if (c.email != null)
                {
                    if (String.Compare((c.email).Trim(), email) == 0)
                    {
                        ret = true;
                        //Send Actual Email
                    }
                }
            }
            if (ret == false)
            {
                foreach (Business_Owner BO in db.Business_Owners)
                {
                    if (BO.email != null)
                    {
                        if (String.Compare((BO.email).Trim(), email) == 0)
                        {
                            ret = true;
                            //Send Actual Email
                        }
                    }
                }
            }

            if (ret == false)
            {
                foreach (System_Manager SM in db.System_Managers)
                {
                    if (SM.email != null)
                    {
                        if (String.Compare((SM.email).Trim(), email) == 0)
                        {
                            ret = true;
                            //Send Actual Email
                        }
                    }
                }
            }
            return ret;
        }

        public static Boolean TryLogin(String username, String password)
        {
            Boolean ret = false;
            foreach (Client c in db.Clients)
            {
                if ((String.Compare((c.username).Trim(), username) == 0) && (String.Compare((c.password).Trim(), password) == 0))
                {
                    LoggedInUser.setClient(c);
                    ret = true;
                }
            }
            if (ret == false)
            {
                foreach (Business_Owner BO in db.Business_Owners)
                {
                    if ((String.Compare((BO.username).Trim(), username) == 0) && (String.Compare((BO.password).Trim(), password) == 0))
                    {
                        LoggedInUser.setBuisnessOwner(BO);
                        ret = true;
                    }
                }
            }

            if (ret == false)
            {
                foreach (System_Manager SM in db.System_Managers)
                {
                    if ((String.Equals((SM.username).Trim(), username)) && (String.Equals((SM.password).Trim(), password)))
                    {
                        LoggedInUser.setSystemManager(SM);
                        ret = true;
                    }
                }
            }
            return ret;
        }

        public static Boolean IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public static Boolean hasNumbers(String str)
        {
            Boolean ret = false;
            for (int i = 0; i < str.Length; i++)
            {
                if (((int)str[i] >= '0') && ((int)str[i] <= '9'))
                    ret = true;
            }
            return ret;
        }

        public static bool checkBusinessSearchTerms(string City, string Cat, string Name)
        {
            Boolean correct = true;
            if ((!IsDigitsOnly(Cat)) || (hasNumbers(City)) || (hasNumbers(Name)))
                correct = false;

            return correct;
        }

        public static List<Business> SearchBusinesses(string City, string Cat, string Name)
        {
            List<Business> toReturn = new List<Business>();

            foreach (Business b in db.Businesses)
                if (((String.Compare(Cat, "") == 0) || (b.category == int.Parse(Cat))) &&
                       ((String.Compare(City, "") == 0) || (String.Compare(b.city, City)) == 0) &&
                       ((String.Compare(Name, "") == 0) || (String.Compare(b.business_name, Name)) == 0))
                    toReturn.Add(b);
            return toReturn;
        }

        public static bool ApproveCoupon(Coupon coupon)
        {
            try
            {
                foreach (Coupon c in db.Coupons)
                {
                    if (c.Serial_Number == coupon.Serial_Number)
                    {
                        c.isApproved = true;
                        db.SubmitChanges();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool TryAddNewBusiness(String BusinessName)
        {
            Boolean exist = false;
            foreach (Business b in db.Businesses)
            {
                if ((!exist) && ((String.Compare((b.business_name).Trim(), BusinessName) == 0)))
                {
                    exist = true;
                }
            }
            return exist;
        }

        public static void addBusiness(string BusinessName, string Address, string City, string Description, string Category)
        {
            Business toAdd = new Business();
            toAdd.business_name = BusinessName;
            toAdd.address = Address;
            toAdd.city = City;
            toAdd.description = Description;
            toAdd.category = int.Parse(Category);
            db.Businesses.InsertOnSubmit(toAdd);
            db.SubmitChanges();
        }

        public static bool TryRemove(Business business)
        {
            Boolean okay = false;
            try
            {
                foreach (Business b in db.Businesses)
                {
                    if (String.Compare(b.business_name, business.business_name) == 0)
                        db.Businesses.DeleteOnSubmit(b);
                }
                db.SubmitChanges();
                okay = true;
            }
            catch
            {
                okay = false;
            }
            return okay;
        }

        public static Boolean checkBusinessInput(String BusinessName, String Address, String City, String Category)
        {
            Boolean correct = true;
            if ((BusinessName == "") || (Address == "") || (City == "") || (Category == "") ||
                (Functions.hasNumbers(City)) || (Functions.hasNumbers(BusinessName)) ||
                (!Functions.IsDigitsOnly(Category)))
                correct = false;

            return correct;
        }

        public static bool TryEdit(Business business)
        {
            Boolean okay = false;
            if (checkBusinessInput(business.business_name, business.address, business.city, "" + business.category))
            {
                try
                {
                    foreach (Business b in db.Businesses)
                    {
                        if (String.Compare(b.business_name, business.business_name) == 0)
                        {
                            b.address = business.address;
                            b.city = business.city;
                            b.description = business.description;
                            b.category = business.category;
                        }
                    }
                    db.SubmitChanges();
                    okay = true;
                }
                catch
                {
                    okay = false;
                }
            }
            
            return okay;
        }
    }
}
