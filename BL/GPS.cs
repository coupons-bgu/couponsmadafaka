﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;
namespace BL
{
    public class GPS : ISensors
    {

        //returns a double array of {longitude, lattitude, GeoPositionAccuracy}
        private double[] getGPSLocation()
        {
            GeoCoordinate gc=new GeoCoordinate();
            double[] ans=new double[3];
            ans[0]=gc.Longitude;
            ans[1]=gc.Latitude;
            ans[2]=gc.HorizontalAccuracy;
            return ans;
        }

        private bool hasAcess()
        {
            GeoCoordinateWatcher gw = new GeoCoordinateWatcher();
            return gw.Permission==GeoPositionPermission.Granted;
        }
        private bool isActive()
        {
            GeoCoordinateWatcher gw = new GeoCoordinateWatcher();
            return gw.Status==GeoPositionStatus.Ready;
        }
        private string getLocation() 
        { 
            GeoCoordinateWatcher gw=new GeoCoordinateWatcher();
            CivicAddressResolver ca=new CivicAddressResolver();
            return ca.ResolveAddress(gw.Position.Location).City;
        }

        public String getData()
        {
            string address = "";
            if (hasAcess())
            {
                if (isActive())
                {
                    address = getData();
                }
                else
                {
                    throw new Exception("Enable the GPS on your device to use the GPS features");
                }
            }
            else
            {
                throw new Exception("Cannot access the GPS on this device. Please check your device's settings");
            }

            return hasAcess().ToString();
        }
    }
}
