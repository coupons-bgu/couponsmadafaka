﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DataBase;

namespace GUI
{
    /// <summary>
    /// Interaction logic for buisnessMenu.xaml
    /// </summary>
    public partial class buisnessMenu : Window
    {
        
        
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0)
            {
                var win = new clientMenu();
                win.Show();
            }
            Close();
        }
        private DataBase.Business business;
        public buisnessMenu(DataBase.Business business)
        {
            this.business = business;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BusinessCoupons BC = new BusinessCoupons(business);
            BC.Show();
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NewCouponWindow NCW = new NewCouponWindow(business);
            NCW.Show();
            Close();
        }
    }
}
