﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace GUI
{
    /// <summary>
    /// Interaction logic for buissnesOwnerMenu.xaml
    /// </summary>
    public partial class buissnesOwnerMenu : Window
    {
        
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

            var win = new Login();
            win.Show(); 
            Close();
        }
        public buissnesOwnerMenu()
        {
            InitializeComponent();
            header.Content = header.Content + LoggedInUser.getName();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword ChangePW = new ChangePassword();
            ChangePW.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            CouponSearch CSearch = new CouponSearch();
            CSearch.Show();
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MyBusinesses businesses = new MyBusinesses();
            businesses.Show();
            Close();
        }
    }
}
