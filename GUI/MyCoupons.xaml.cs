﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MyCoupons.xaml
    /// </summary>
    public partial class MyCoupons : Window
    {
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0)
            {
                var win = new clientMenu();
                win.Show();
            }
            Close();
        }
        public MyCoupons()
        {
            InitializeComponent();
            CouponGrid.ItemsSource = Functions.generateCoupons();
            CouponGrid.Visibility = Visibility.Hidden;
            CouponGrid.SelectionMode = DataGridSelectionMode.Single;
            CouponGrid.IsReadOnly = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (CouponGrid.Columns.Count > 10)
            {
                CouponGrid.Columns.RemoveAt(11);
                CouponGrid.Columns.RemoveAt(10);
            }
            CouponGrid.Visibility = Visibility.Visible;
            
        }
    }
}
