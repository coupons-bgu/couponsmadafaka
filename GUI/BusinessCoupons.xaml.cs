﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DataBase;

namespace GUI
{
    /// <summary>
    /// Interaction logic for BusinessCoupons.xaml
    /// </summary>
    public partial class BusinessCoupons : Window
    {
        
        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0)
            {
                var win = new clientMenu();
                win.Show();
            }
            Close();
        }
        private DataBase.Business business; 

        public BusinessCoupons(DataBase.Business business)
        {
            this.business = business;
            InitializeComponent();
            BusinessGrid.ItemsSource = Functions.generateCouponsForBusinesses(business);
            BusinessGrid.Visibility = Visibility.Hidden;
            BusinessGrid.IsReadOnly = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (BusinessGrid.Columns.Count > 10)
            {
                BusinessGrid.Columns.RemoveAt(11);
                BusinessGrid.Columns.RemoveAt(10);
            }
            BusinessGrid.Visibility = Visibility.Visible;
        }
    }
}
