﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

namespace GUI
{
    /// <summary>
    /// Interaction logic for AddBusiness.xaml
    /// </summary>
    public partial class AddBusiness : Window
    {
        public AddBusiness()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String msg = "";
            Boolean correct = Functions.checkBusinessInput(BusinessName.Text, Address.Text, City.Text, Category.Text);
            if (correct)
            {
                Boolean exist = Functions.TryAddNewBusiness(BusinessName.Text);
                if (exist)
                    msg = "Business name already exist. Please try again.";
                else
                {

                    Functions.addBusiness(BusinessName.Text, Address.Text, City.Text, Description.Text, Category.Text);
                    msg = "Business name created successfully.";
                    Close();
                }
            }
            else
                msg = "Invalid parameters. Try again.";
            MessageBox.Show(msg);

        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0) {
                var win = new clientMenu();
                win.Show();
            }
            
            Close();
        }
    }
}
