﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DataBase;

namespace GUI
{
    /// <summary>
    /// Interaction logic for NewCouponWindow.xaml
    /// </summary>
    public partial class NewCouponWindow : Window
    {
        
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0)
            {
                var win = new clientMenu();
                win.Show();
            }
            Close();
        }
        private Business business;

        public NewCouponWindow()
        {
            InitializeComponent();
            ChooseBusiness.Visibility = Visibility.Visible;
            Businesses.Visibility = Visibility.Visible;
        }

        public NewCouponWindow(Business business)
        {
            this.business = business;
            InitializeComponent();
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            // ... A List.
            List<string> data = new List<string>();
            data.Add("1");
            data.Add("2");
            data.Add("3");
            data.Add("4");
            data.Add("5");

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = data;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;
        }

        private Boolean checkInput(String cat, String CatID, String Name, String Desc, String OPrice, String DPrice, String SNumber)
        {
            Boolean correct = true;
            if ((cat == "") || (DPrice == "") || (!Functions.IsDigitsOnly(cat)) ||
                (!Functions.IsDigitsOnly(CatID)) || (!Functions.IsDigitsOnly(OPrice)) ||
                (!Functions.IsDigitsOnly(DPrice)) || (!Functions.IsDigitsOnly(SNumber)) ||
                (SNumber == "") || (Name == "") || (CatID == "") || (OPrice == ""))
                correct = false;
            else if (int.Parse(DPrice) > int.Parse(OPrice))
                correct = false;
            return correct;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime LastDate = DateTime.Now;
            String msg = "";
            Boolean correct = checkInput(cat.Text, CatID.Text, CName.Text, Desc.Text, OPrice.Text, DPrice.Text, SNumber.Text);
            try
            {
                LastDate = (DateTime)LD.SelectedDate;
            }
            catch
            {
                MessageBox.Show("Please pick a date");
            }
            if ((correct) && (LastDate > DateTime.Now))
            {
                Boolean exist = Functions.TryAddNewCoupon(SNumber.Text);
                if (exist)
                    msg = "Serial Number is taken. Please Try Again";
                else
                {
                    business = (Functions.SearchBusinesses("", "", Businesses.Text)).First();
                    Functions.addCoupon(cat.Text, CatID.Text, CName.Text, Desc.Text, OPrice.Text, DPrice.Text, SNumber.Text, Rating.Text, LastDate, business);
                    msg = "Coupon created successfully.";
                    Close();
                }
            }
            else
                msg = "Invalid parameters. Try again.";
            MessageBox.Show(msg);

        }

        private void Businesses_Loaded(object sender, RoutedEventArgs e)
        {
            List<String> list = new List<String>();
            foreach (Business b in Functions.generateBusinesses())
            {
                list.Add(b.business_name);
            }
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            Businesses.ItemsSource = list;

            // ... Make the first item selected.
            Businesses.SelectedIndex = 0;
        }
    }
}
