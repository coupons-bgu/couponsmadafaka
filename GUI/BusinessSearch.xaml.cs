﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DataBase;

namespace GUI
{
    /// <summary>
    /// Interaction logic for BusinessSearch.xaml
    /// </summary>
    public partial class BusinessSearch : Window
    {
        
        
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var user = LoggedInUser.getUserType();
            if (user == 1)
            {
                var win = new buissnesOwnerMenu();
                win.Show();
            }
            if (user == 2)
            {
                var win = new systemManagerMenu();
                win.Show();
            }
            if (user == 0)
            {
                var win = new clientMenu();
                win.Show();
            }
            Close();
        }
        public BusinessSearch()
        {
            InitializeComponent();
            Expand.IsEnabled = false;
            BusinessGrid.ItemsSource = Functions.generateBusinesses();
            BusinessGrid.Visibility = Visibility.Hidden;
            BusinessGrid.SelectionMode = DataGridSelectionMode.Single;
            BusinessGrid.IsReadOnly = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Expand.IsEnabled = false;
            if (Functions.checkBusinessSearchTerms(City.Text, Cat.Text, BusinessName.Text))
                BusinessGrid.ItemsSource = Functions.SearchBusinesses(City.Text, Cat.Text, BusinessName.Text);
            else
                MessageBox.Show("Invalid input on search. Please try again.");
            if (BusinessGrid.Columns.Count > 5)
            {
                BusinessGrid.Columns.RemoveAt(6);
                BusinessGrid.Columns.RemoveAt(5);
            }
            BusinessGrid.Visibility = Visibility.Visible;

        }

        private void Expand_Click(object sender, RoutedEventArgs e)
        {
            if (BusinessGrid.SelectedItem.GetType() == typeof(DataBase.Business))
            {
                BusinessCoupons BC = new BusinessCoupons((DataBase.Business)BusinessGrid.SelectedItem);
                BC.Show();
                Close();
            }
            else
                MessageBox.Show("Please select a real buisness.");
        }

        private void BusinessGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Expand.IsEnabled = true;
        }
    }
}
