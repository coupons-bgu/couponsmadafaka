﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using NUnit.Framework;
using main;

namespace tests
{
    [TestFixture]
    class Tests
    {
        

        [Test]
        public void addManager(){
            DatabaseDataContext db = new DatabaseDataContext();
            System_Manager manager = new System_Manager();
            manager.birth_date = new DateTime(1985, 6, 6);
            manager.first_name = "david";
            manager.last_name = "ben david";
            manager.password = "qwerty";
            manager.email = "dvd@gmail.com";
            manager.phone = "0501239874";
            manager.username = "bendavid";

            int actual = db.System_Managers.Count();
            int expected = actual + 1;
            db.System_Managers.InsertOnSubmit(manager);

            db.SubmitChanges();
            actual = db.System_Managers.Count();
            Assert.AreEqual(expected,actual);

        }
        [Test]
        public void deleteManager()
        {
            {
                DatabaseDataContext db = new DatabaseDataContext();
                var result = (from a in db.System_Managers
                              where a.username == "bendavid"
                              select a).First();
                int actual = db.System_Managers.Count();
                int expected = actual - 1;
                db.System_Managers.DeleteOnSubmit(result);
                db.SubmitChanges();
                actual = db.System_Managers.Count();
                Assert.AreEqual(expected, actual);

            }

        }
        [Test]
        public void checkName()
        {
            DatabaseDataContext db = new DatabaseDataContext();
            var result = (from a in db.Clients
                          where a.username == "oripapo"
                          select a).First();
            String actual = result.first_name.ToString();
            actual = actual.Substring(0, 3);
            Assert.AreEqual("ori", actual);
        }

        [Test]
        public void changeName()
        {
            DatabaseDataContext db = new DatabaseDataContext();
            var result = (from a in db.Clients
                          where a.username == "oripapo" 
                          select a).First();
            String temp = result.first_name;
            result.first_name = "nameChange";
            cli.Submit(db);
            
            result = (from a in db.Clients
                          where a.username == "oripapo"
                          select a).First();
            Assert.AreEqual("nameChange", result.first_name);
            result.first_name = temp;
            db.SubmitChanges();
        }

        [Test]
        public void addCoupon()
        {
            DatabaseDataContext db = new DatabaseDataContext();
            Coupon VacuumCoupon = new Coupon();
            VacuumCoupon.categoryID = 22;
            VacuumCoupon.couponCategory = 2;
            VacuumCoupon.description = "the best vacuum in the world!";
            VacuumCoupon.discountedPrice = 80;
            VacuumCoupon.isApproved = true;
            VacuumCoupon.lastDate = new DateTime(2015, 11, 5);
            VacuumCoupon.name = "vacuum";
            VacuumCoupon.orginialPrice = 100;
            VacuumCoupon.rating = 5;
            VacuumCoupon.Serial_Number = 321;

            
            int actual = db.Coupons.Count();
            int expected = actual + 1;
            db.Coupons.InsertOnSubmit(VacuumCoupon);

            db.SubmitChanges();
            actual = db.Coupons.Count();
            Assert.AreEqual(expected, actual);
           }

        [Test]
        public void deleteCoupon() {
                DatabaseDataContext db = new DatabaseDataContext();
                var result = (from a in db.Coupons
                              where a.Serial_Number == 321
                              select a).First();
                int actual = db.Coupons.Count();
                int expected = actual - 1;
                db.Coupons.DeleteOnSubmit(result);
                db.SubmitChanges();
                actual = db.Coupons.Count();
                
                Assert.AreEqual(expected, actual);
            }

        [Test]
        public void addClient(){
            DatabaseDataContext db = new DatabaseDataContext();
         Client ori = new Client();
            ori.birth_day = new DateTime(1991, 5, 1);
            ori.first_name = "ori";
            ori.last_name = "papo";
            ori.password = "9440990";
            ori.email = "abc@gmail.com";
            ori.phone = "0508626501";
            ori.username = "oriTest";
            
            int actual = db.Clients.Count();
            int expected = actual + 1;
            db.Clients.InsertOnSubmit(ori);

            db.SubmitChanges();
            actual = db.Clients.Count();
            Assert.AreEqual(expected, actual);
            }

        [Test]
        public void deleteClient() {

            DatabaseDataContext db = new DatabaseDataContext();
            var result = (from a in db.Clients
                          where a.username == "oriTest"
                          select a).First();
            int actual = db.Clients.Count();
            int expected = actual - 1;
            db.Clients.DeleteOnSubmit(result);
            db.SubmitChanges();
            actual = db.Clients.Count();

            Assert.AreEqual(expected, actual);
            
        }

        [Test]
        public void addBusiness()
        {
            DatabaseDataContext db = new DatabaseDataContext();
            Business business = new Business();
            business.address = "Rager 12";
            business.business_name = "sweetshop";
            business.category = 2;
            business.city = "beer sheva";


            int actual = db.Businesses.Count();
            int expected = actual + 1;
            db.Businesses.InsertOnSubmit(business);

            db.SubmitChanges();
            actual = db.Businesses.Count();
            Assert.AreEqual(expected, actual);
 
        }
        [Test]
        public void deleteBusiness()
        {

            DatabaseDataContext db = new DatabaseDataContext();
            var result = (from a in db.Businesses
                          where a.business_name == "sweetshop"
                          select a).First();
            int actual = db.Businesses.Count();
            int expected = actual - 1;
            db.Businesses.DeleteOnSubmit(result);
            db.SubmitChanges();
            actual = db.Businesses.Count();

            Assert.AreEqual(expected, actual);
            
        }
        }


    }
        


